'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {


  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', ['$http', '$scope', function($http, $scope) {
    $http({
        method: 'GET',
        url: 'http://localhost:8080/user/'
    }).then(function successCallback(response) {
        // this callback will be called asynchronously
        // when the response is available
        $scope.allUsers = response.data;
        console.log(response.data);
    }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
    });



}]);